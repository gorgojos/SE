/*
  ReDuino.h - Main include file for the ReDuino SDK
  Copyright (c) 2013-2018 ReDuino Team.  All right reserved.
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#ifndef ReDuino_h
#define ReDuino_h

#define si if
#define siNo else
#define escrituraDigital(A , B) digitalWrite(A , B)
#define escrituraAnalogica(A , B) analogWrite(A , B)
#define lecturaDigital(A) digitalRead(A)
#define lecturaAnalogica(A) analogRead(A)
#define modoPata(A , B) pinMode(A , B)
#define SALIDA OUTPUT
#define ENTRADA INPUT
#define ENTRADA_RESISTENCIA_ALTO INPUT_PULL_UP

#define ALTO HIGH
#define BAJO LOW

#define verdadero true
#define falso false


#define sinSigno unsigned
#define conSigno signed


#define entero int
#define caracter char

#define real float
#define dobleReal double

#define binario boolean
#define booleano boolean



#define largo long 
#define corto short 


#define inicio {
#define fin }

#define nada void
#define configuracion setup
#define bucle loop

#endif
